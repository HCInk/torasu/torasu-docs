![(TORASU LOGO)](logo/TorasuLogo2TextBannerColor.svg "TORASU-Logo")

<a href="https://discord.gg/hercMEF" title="Official HCInc Discord Community"> ![Official HCInc Discord Community](https://discordapp.com/api/guilds/504192138158931968/widget.png?style=shield) </a>

# TORASU guide and documentation

Welcome to the guide and documentation the TORASU Compute Framework Project!

## What is TORASU?

TORASU is a project to enable agile and modular operations in an abstract but (hopefully) easy way.

Also T.O.R.A.S.U. is a shortcut for the main focuses of the project:

- **T**argeted - Operations done in torasu are managed by targets
- **O**bjective - Computations are accomplished using an Objective structure to improve expandability
- **R**obust - We offer many options to test on failing components and also have strong exception handling at runtime if you wish
- **A**utomated - The Process of computation aims to be fully automatable and procedural
- **S**calable - The architecture of TORASU enables "easy" scalability
- **U**nified - The Objects used, have a unified structure that enables extreme modularity
- **Compute Framework** - Well we are here for computation, aren't we?

## Where to start?

Wow, already interested? 

Thanks for your interest, but i would still recommend some waiting, we are still working on the basics and on making the project more understandable to newcomers.
