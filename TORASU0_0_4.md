TORASU Compute Framework 0_0_4 Notes
==========================




# Core Definitions



## Element

Elements are the default and recommended object type for computation trees. They can be put into a state of "ready", if that state applies the object is seen as "ready for use" in the tree - the concrete definition is further defined by derived classes of Element (for example a `Renderable` needs to be ready to execute `render`). To put "ready" into perspective the state "ready" is similar to "loaded" in other applications. The extent of the ready-ness is defined by a `RenderContext` that is given upon loading.

Elements can also have dependencies with other objects, which need to be made ready before, these dependencies consist of the element that is the dependencies and the `RenderContext` that describes the extent. Those dependencies can also be cascaded, which means some dependencies need to be made ready to receive the next dependencies from the Object. (Info: Elements which don't require cascading still return cascaded dependence objects, but the size of the cascade is set to 1 so it worked just as a list of dependencies without re-fetching)

- Made cloneable via a `Cloneable` implementation
- Made portable by being a `DataPackable`

DataPackable-structure:

```
{
	ident: "e",	// Identifier of a Element-DataPackable
	ei: "TORASU::RMULTIPLY",	// Identifier of the Element-type (here: the Renderable "RMultiply")
	d: null,	// DataResource which is providing the static data to the Element (here: none/null)
	c: {	// Other Elements connected as inputs (here: the two values to be mutliplied - a*b=c)
		a: {
			// Another Element structure (here: connected as the a-value of the multiplication)
		},
		b: {
			// Another Element structure (here: connected as the b-value of the multiplication)
		}
	}
}
```

Make an Element ready:

0 - (Ensure the element is instantiated)

1 - Create a ready-session via `readySession(...)` - If this returns as an readyId `-2` there is nothing to be made ready for these operations in that `RenderContext`

2 - Make the dependencies ready, that are returned by `dependencies(...)`

3 - Make the Element itself ready via `readySelf(...)`

4 - The element is now ready for use

Update the ready-state of an Element:

*(this requires to have made the Element ready before)*

1 - Update the `RenderContext` via `readySessionUpdate(...)` to update the values of the ready session, set `updateValues` to true if the values in the `RenderContext` have changed and not (only) the modifications

2 - Make the dependencies ready, that are returned by `dependencies(...)`

3 - Make the Element itself ready via `readySelf(...)`

4 - The element is now ready for use with the new setting again

=> NOTE: The steps 2 and 3 can be limited to only the required actions in `ReadyRefreshPolicy`

Cascaded dependencies are made ready like this:

1 - Get first/next cascade of the Element, by with the current cascade index (index starts at 0).

2 - Ready those dependencies

3 - Check `DependenceStack` if another cascade is available (if yes increase index by one and get back to 1.)

4 - When all cascades are ready, ready the element itself.

Another part of the content of an Element are the `PipelineSettings`, which define usable implementations, algorithms and attributes that are recognized by the pipeline.

Moreover that Elements also contain some metadata methods, like `name` and `info`: These are used for orientation of a user in the data.

Methods of `Element` (without `Cloneable`):

- `getName()` -> String - Name tag for user-orientation
- `getInfo()` -> String[] - Multi-line info for user-orientation
- `readySession(RenderContext rctx, String[] operations)` -> `long` (`readyId`) - Create a session to make the Element ready, referenced by the `readyId` (remove the session's data via `unready(long readyId)`) 
- `readySessionUpdate(long readyId, RenderContext rctx, boolean updateValues)` - Update the session context
- `readySelf(long readyId, int pld, SystemResourceInterface sysri, ExecutionInterface ei)` -> `ReadyRefreshPolicy` - Making the Element "ready" - eg. load data
- `unready(long readyId)` - Remove the Element from the state "ready" it was loaded into before (use the `readyId` that got returned earlier) - e.g. unload data / remove the loaded data from memory
- `isReady(RenderContext rctx, String[] operations)` -> `boolean` - indicates if the Element is ready (should always be true after the dependencies are worked down as described and no error was caused while making ready)
- `dependencies(long readyId, double weightIndex, int index, ExecutionInterface ei)` -> `DependenceStack` - fetches dependencies (used in in the example for making cascaded dependencies ready)
- `getRenderContextMask(String[] segments)` -> `RenderContextMask` - Get what values of a `RenderContext` are going to get used methods

=> The `pld` (`pld ∈ ℕ≥0`) (pre-load-depth) defines a set of available methods on an `Element`. `pld=0` means all methods have to be loaded, the higher the pre-load-depth the less methods have to be loaded, which methods can be left out at which pld defines the type of the `Element` (for example the `Renderable` has a definition for plds). It just has to follow the rule, that that the more methods are implemented are the closer the `pld` gets to 0 and when `pld=0` then all methods should be ready. (`pld < 0` is invalid)

=> The `weightIndex` (Normal: `0 ≤ weightIndex ≤ 2`, Preload: `-maxPLD ≤ weightIndex < 0`) defines how much of the dependencies should be put out: 0 = Only Elements that are required to be ready to run the methods; 1 = The dependencies that should be made ready for optimal performance (at least loosely guessed); 2 = Elements to be made ready for the most frictionless operation as possible (NOTE: A run of weightIndex=0 is always guaranteed (except for preloading), the higher the weightIndex is the more dependencies added); Negative Values: Those define the pld, which is calculated like this `weightIndex*-1=pld`. They don't offer any fine tuning of the `weightIndex`, this means that only the minimum dependencies for the current pld should be returned.

Note: the string array (`String[] operations`) is the array of operations that need to be made ready, those operations look Identifier-like - under `Renderable`s those are equivalent to the identifier for the render-segment - declaring those operations is optional and is just to avoid readying things that don't have to be ready - if everything has to be made ready a `null` can be passed

Methods of `DependenceStack`:

- `getDependencies()` -> `Dependence[]` - Get dependencies in the current stack
- `getIndex()` -> `int` - Get the index of the current cascade (index starts at 0)
- `getSize()` -> `int` - Get how many cascades are there in total
- `getFullRefreshPolicy()` -> `ReadyRefreshPolicy` - Get the `ReadyRefreshPolicy` for the whole cascade (cascade size also may change - only contained at index=0)
- `getStackRefreshPolicy()` -> `ReadyRefreshPolicy` - Get the `ReadyRefreshPolicy` for the current stack.

Methods of `Dependence`:

- `getElement()` -> `Element` - Get the `Element` that the dependence refers to
- `getRenderContext()` -> `RenderContext` - Get the `RenderContext` the `Element` should be ready for
- `getOperations()` -> `String[]` - Get the array of operations the `Element` should be ready for
- `getPld()` -> `int` - Get the minimum pre-load-depth the `Element` should be ready for

Methods of `ReadyRefreshPolicy`:

- `getNextHigherWI()` - `double` - Contains the the next higher `weightIndex` (or contained `pld`) it should be re-ran/refreshed for
- `isOnValue()` -> `boolean` - Re-run/refresh if a value in the `RenderContext` has changed
- `isOnModification()` -> `boolean` - Re-run/refresh if a modification in the `RenderContext` has changed 

### Rewritable

Provides an interface for the `Element` to edit/override the content of an element. *Note: If the element is an `IDable` the version of the element should increment on the next access, except a version got set afterwards by `setVersion(long newVers)`*

Implementation of `Rewritable`:

- `rewrite(DataPackable data, Map<String, Element> elements)` - Rewrites the element-data
- `overrideElement(String key, Element elem)` -> `int` - Replaces an element inside the element - Returns a status code: 0: OK; <0: Error


### IDable

The `IDable` provides an interface, which is made to re-identify an `Element` and its version.

Implementation of `IDable`

- `getID()` -> `long` - unique ID of the element
- `getVersion()`-> `long` - version number of the element (numbered in an ascending scheme) - indicates if the objects has changed
- `getVersionSatck()` -> `long[]` - unique version-stack of the element (it has no special formatting, it just has to be different if the children have changed)
- `setVersion(long newVers)` -> void - Set the version of element - NOTE: This should only done by administrative functions, as when loading a newer element-state with a certain version-number



## Renderable

- The `Renderable` is an Element, which can generate a result
- Which result should be generated is defined in the `ResultSettings`
- Renderables can receive mandatory modifications via the `RenderContext`
- Renderables can promise integrated calculations called `RenderModifier`
- Since Renderables are derived from Elements, so they have the same Ready/Dependence/Metadata/etc features

>___
>GENRAL RULE FOR RENDERABLES:
>**SAME INPUT = SAME RESULT**
>
>- if the `RenderContext` is the same and `ResultSettings` are the same, it has to return the same result
>- If the ResultSettings changes, the `Renderable` should also reproduce the most similar result for those `ResultSettings`
>- Since Renderables can have multiple result-classes: An exception is when there is an entirely different result classes (for example from image to sound) defined in the `ResultSettings`, then it should give the according result for that class
>___

Methods of `Renderable` (not including derived ones from `Element`):

- `render(RenderInstruction ri)` -> `RenderResult` - (Only when Ready) Renders the `Renderable` to the information in the `RenderInstruction`, which contains the `RenderContext` and `ResultSettings`, as well as the `SystemResourceInterface` and `ExecutionInterface`
- `getProperties(String[] keys, RenderContext rctx)` -> `RenderableProperties` - (When Ready: More Info / Information change) Gives the requested data about the `Renderable` (**NOTE:** Make sure to ready the fitting operations to the keys)
- `promiseRenderModification(String[] requests, RenderContext rctx)` -> `String[]` - (Only when Ready) Idenificators of the types of `RenderModifier` that can be applied over the `RenderContext` - Arguments are the identificators of the requested modifications that are requested to get promised and the current `RenderContext` the promises should happen on. - Those promises may not change after other `RenderModifier` have been added to the `RenderContext`, all promised `RenderModifier`(s) have to be able to be applied at the same time. (**NOTE:** Make sure to ready the fitting operations to the requests)
- `getExecutionRequirements(RenderContext rctx, String[] segments)` -> `ExecutionRequirements` - Returns the `ExecutionRequirements` for the `Renderable`
- `getPipelineSettings()` -> `PipelineSettings` - Gets `PipelineSettings` for this renderable, which contains information on algorithm picking and attributes recognized by pipleline modules

=> pre-load-depth (`pld ∈ ℕ≥0; pld ≥ 2`) information for Renderables should be interpreted as follows: 

- 0 = All methods ready - Render is ready, everything can be called
- 1 = Render not ready, everything can be called. (Usually used before the RenderModifiers have been applied over `promiseRenderModification(...)`)
- 2 = Render not ready and RenderModifiers also can't be applied yet (Usually called before the data hasn't been loaded yet or the `RenderContext` is still not fully determined)

#### Executing a render of a Renderable

0 - (Instantiate Object if not instantiated already)

1 - Get the `RenderContextMask` via `getRenderContextMask(String[] segments)` - Get the mask on how we are able to mask the `RenderContext` in the render proccess to save data

2 - Make sure the `Renderable` is ready (for the operations)

3 - Get the `RenderableProperties` via `data(String[] keys, RenderContext rctx)` if needed to determine more the exact arguments for the render. (`pld` has to be at least 2 by now)

4a - If the render context has changed: please make sure to unready the old context again, when it has been readied in while getting the data (if the readiness is not required in another execution) - If data has to be fetched again, skipping back to 1 is possible, but be be aware not to get stuck in a loop

4b - If everything is clear data-wise then we can continue to step 5

5 - Now we can check from modification-promises via (`promiseRenderModification(String[] requests, RenderContext rctx)`) and implement some that we want to use, into the `RenderContext` - We don't have to fetch the data again since it is only allowed to change about the value-part in the `RenderContext`, and is always the same no matter what is noted in the modifications there. (the `pld` has to be at least 1 by now)

6 - Finally make sure that the `Renderable` is ready even with the new and final `RenderContext` - Even though the renderable-data hasn't changed from the changes on the `RenderModifier`(s), we have still have to make it ready to apply those modifiers. - If no changes had been done to the context and the renderable is already ready to it we can skip this step.

7 - Render the Renderable with the `RenderContext` over `render(RenderInstruction ri)` and use the result as an input. (the `pld` has to be at least 0 by now)

8 - Remember to un-ready the element if unneeded for a while, to save resources

Note: Steps 0,2,6 and 7 are mandatory for the render. Steps 3 4a/4b are required if we do access to the data of the renderable. The 5th step has to be done if we work with promises for one/multiple `RenderModifier`(s). Step 8, while it is not not mandatory to the functioning in theory, is still strongly recommended to save memory. - Step 1 is also optional, but is to save on the size of the `RenderContext`-Object, by masking it accordingly

### ResultSettings

`ResultSettings` are the definition of what is result is expected from the `Renderable`. It contains an array of `ResultSegmentSettings` to define settings for the individual segments.

Implementation of `ResultSettings`:

- `getSegments()` -> `ResultSegmentSettings[]` - Get the settings for the individual segments that should be rendered.

TODO More info on Import/Export of the `ResultSettings`

#### ResultSegmentSettings

The `ResultSegment` defines a segment of the result for (For example a visual track or a channel of audio).

Values that make up the settings of a segment:

- The segment key: It is used to find back the Segment later on in the `RenderResult`
- The segment identifier: An Identifier, which describes the what result this segment is defining (For example a visual track or a channel of audio)
- An array of `ResultFormatSettings` in order of priority

If the `Renderable` can't reach the given render-segment and (one of) the given format(s), the render will fail (for this segment). But if the class/format specific data fail, there will be format-specific handling - just be sure, that the expected result has to be generated, otherwise it is strongly recommended to fail. 

Implementation of `ResultSegmentSettings`:

- `getKey()` -> `String` - Segment key to identify the part in the returned `ResultSegmentSettings`
- `getIdent()` -> `String` - Get (usually standardized) identifier for the segment (Example: The key for the default IMGC-2DVisual render-segment is:  `de.hcinc.imgc.rseg.tdvis`)
- `getResultFormatSettings()` -> `ResultFormatSettings[]` - Available formats for that Result Segment in an order of priority

#### ResultFormatSettings

Defines a possible result-format of the format of a `ResultSegment`.

- Identifier for the result-format (for example `de.hcinc.imgc.res.stdbimg`)
- Sub-format for result (For example the format  `de.hcinc.imgc.res.stdbimg` can have the sub-format `RGBA`, those sub-formats can also be stacked like `RGBA`/`HDR`)
- Information for the result-format... a map of properties in the `DataPackable` format

> Definition: **Format/Sub-Format/FormatProperties**
>
> - **Format**: Basic format implementation - equivalent to an object-class (Example: `de.hcinc.imgc.res.stdbimg`) - Although they are imported into the same object-class, there can be different importers used 
> - **Sub-format**: Setting for the format implementation (Example: `RGBA`) - If a sub-format is marked as supported all formats having the same sub-format are promised to be imported, while all format FormatProperties have to be supported the same
> - **FormatProperties**: Properties for the format, that are saved in the `DataPackable` and contain custom properties regarding the specific (sub-)format

Implementation of `ResultFormatSettings`:

- `getFormatIdent()` -> `String` - Gets the Identifier of the Format defined
- `getSubFormats()` -> `String[]` - Get array that is defining the sub-format
- `getFormatData()` -> `DataPackable` - Get format-specific data


### RenderInstruction

The `RenderInstruction` is a collection of data required to run a render, containing the `RenderContext` and `ResultSettings` as usable data, but also containing an `SystemResourceInterface` for system-resource management and a `ExecutionInterface` to manage child renders/target-executions

Methods of `RenderInstruction`:

- `getRenderContext()` -> `RenderContext` - Gets the render-context of the render
- `getResultSettings()` -> `ResultSettings` - Gets the result-settings of the render
- `getSystemResourceInterface()` -> `SystemResourceInterface` - Gets the interface for system-resource management of the render
- `getExecutionInterface()` -> `ExecutionInterface` - Gets the interface to render child-renderables or execute child-targets
- `getEnabledAlgorithms()` -> `String[]` - Get the identifiers of the available ElementAlgorithms (as defined in `AlgorithmMap`)
- `getLogInstruction()` -> `LogInstruction` - Get the log-instruction for the render


### RenderResult

The `RenderResult` delivers the result of a render, errors and other data about the result.

It contains:

- The result of the render (if no error occurred)
- Status code
- Error information

Implementation of `RenderResult`:

- `getStatus()` -> `int` - Get the status of the render 0=OK, -5=Internal Error, -1=Unknown error   (Generally: <0 = Error: Result (partially) Invalid)
- `getResult()` -> `ResultSegment[]` - Get the Result segments defined in the `ResultSettings`
- `getAppModifiers()` -> `String[]` - Get the applied `RenderModifier`(s), to validate that all modifiers that were promised and put got applied.

#### ResultSegment

A `ResultSegment` is the result of the render-process, which was configured by the `ResultSegmentSettings` in the `ResultSettings`

- The key that result-segment got mapped to in the `ResultSettings`/`ResultSegmentSettings`
- Contains the identifier of the result-format
- The actual result
- *Note: Information like sub-formats and format-specific data are not stored here - they have to be available through methods in the result object if they need to get accessed afterwards*

Implementation of `ResultSegment`:

- `getKey()` -> `String` - Gets the key that result-segment got mapped to in the `ResultSettings`/`ResultSegmentSettings`
- `getFormatIdent()` -> `String` - Gets the Identifier of the format contained in `getResult()`
- `getResult()` -> `Object` - Gets the result configured by the `ResultSegmentSettings` in the `ResultSettings`


### RenderableProperties

`RenderableProperties` supplies information about a `Renderable`. (For example the resolution for images, the sample-rate for audio or the range of drivers)

The Object `RenderableProperties` contains a map of properties of the `Renderable` that can be read by other operations such as a `Target` and other another `Renderable`.

The map is keyed by strings which are recommended to look identifier-like .. the the values can be user-defined and are implemented as `DataPackable` to make portability possible.

Implementation of  `RenderableProperties`:

- `get(String key)` -> `DataPackable` - The custom value put on the key. (Returns null if nonexistent)
- `getJson()` -> `JSONObject` - Get the renderable data as a Full JSON Object to export


### Difference between rendering and making ready

Readying is to prepare Elements for the generation of the results, render on the other hand is meant to create the result - The technical difference is here, that render generates a result, where the readying loads/generates data in the Element



## RenderContext

The `RenderContext` defines the situation, a render is in. It can describe basically anything concerning the context of a render.
A few examples are: time, shutter setting, lens, a user defined variable/color, etc...

The render context splits into two datasets;

- The RenderContextValue-Map contains attributes which are values, which are distributed through the render process (For example a position on a 2D-Area that has been marked or a color that has been defined). Those values are called `RenderContextValue`, but they are stored as `RenderContextObject` in the map to contain extra information. - The keys in that map can be user or defined by a Standard/Framework, if its defined by a standard or framework the key usually looks Identifier-like and if it is defined by a user it rather looks like a normal string.
- The RenderModifier-List contains attributes, which are modifications on what operations are done while rendering (For example a 2D-Transformation that has to be added while rendering). Those the values here are called `RenderModifier` - Those modifications have to be promised beforehand by the `Renderable`, otherwise they can't be applied - Same is when the Renderable promised the modifications, it has to do the modifications, otherwise the render will be invalidated. - Those Modifiers also only apply the `Renderable` that is executed and aren’t passed down as a `RenderContextValue`/`RenderContextObject`. The modifiers may be passed, but reviewed manually to obtain the same result as it would be applied by itself. An Exception are Renderables only modify the `RenderContextValue` and/or the `RenderableInformation` and pass all other methods of the `Renderable` functionality over. - A `RenderModifier` that has been processed has to be returned in the `RenderResult` to validate the modification.

It can also be exported as a JSON (via `getJson()`) - Example:

~~~
{
    //Map of `RenderContextValue`/`RenderContextObject`
    vals: {
    	"customPos": {
            //Identifier of the RenderContextValue (Content of getIdent() )
            ident: "de.hcinc.rctx.val.TwoDCordValue",
            //Data of the RenderContextValue (Content of getValJSON() )
            data: {x: 0.1, y: -0.2},
            //The transformations that have to be noted (Content of getWritableTransformations() )
            wtr: ["de.hcinc.rctx.tr.ScaleOffsetTransofrmation"],
            //The array of transformations in chronological order (Content of getTransformations() )
            tr: [
                {
                    ident: "de.hcinc.rctx.tr.ScaleOffsetTransofrmation",    //Identifier of the Transformation
                    data: {xs: 2, xy: 2, xo: 0.1, yo: 0.3}                  //Data of the Transformation
                }
            ]
        }
    },
    //List of `RenderModifier`
    mods: [
        {
            ident: "de.hcinc.rctx.mod.AffineMatrixTranformation",           //Identifier of the RenderModifier
            data: {m: [1.5, 0.25, 0, 0.25, 1.5, 0]}                         //Data of the RenderModifier
        }
    ]
}
~~~

Implementation of `RenderContext`:

- `getValueObjects()` -> `Map<String, RenderContextObject>` - Gets the RenderContextValue-Map
- `getModifiers()` -> `RenderModifier[]` - Get the RenderModifier-List
- `getJson()` -> `String` - Export the `RenderContext` as a JSON


### RenderContextObject

The `RenderContextObject` contains pipelined information about a `RenderContextValue` that is stored in a `RenderContext`

The Object basically has three fields:

1. The field with the actual `RenderContextValue`, with direct values or a json-string that describes that value.
2. An array of `RenderContextTransformationInformation` which describe how the value had been transformed - this transformation has to be reverse-applied on the current value stored to match the result - coming first in the array are the oldest transformations and at the end come the latest transformation
3. Which kind of transformations even have to be reverse-applied. Respectively which transformations are added to the array of `RenderContextTransformationInformation`

**IMPORTANT NOTE:** To obtain the `RenderContextValue` that is bundled, it is strongly recommended to use the method `unpackRenderContextValue(RenderContextObject rctxo)` of the `ExecutionInterface` to automatically handle the transformation(s) or even decode the json, if no decoded object is present.

Implementation of `RenderContextObject`:

- `getValue()` -> `DataPackable` (symbolizes `RenderContextValue`) - Get the un-applied bundled value from the `RenderContextObject` - **IMPORTANT NOTE:** To obtain the `RenderContextValue` that is bundled, it is strongly recommended to use the method `unpackRenderContextValue(RenderContextObject rctxo)` of the `ExecutionInterface` to automatically handle the transformation(s) or even decode the json, if no decoded object is present.
- `getJson()` -> `String` - Generates the JSON data to symbolize the current `RenderContextObject`
- `getIdent()` -> `String` - Get the Identifier for the type of the bundled `RenderContextValue`
- `getTransformations()` -> `RenderContextTransformationInformation[]` - Get transformations which are pending to be applied
- `getWritableTransformations()` -> `String[]` - Gets an array of the identifiers of transformations that should be written
- `writeTransformation(RenderContextTransformationInformation trans)` - Write a transformation here, if noted in the array of `getWritableTransformations()` contains the ident of the transformation done.


JSON export example (via `getJson()`):

~~~
{
    //Identifier of the RenderContextValue (Content of getIdent() )
    ident: "de.hcinc.rctx.val.TwoDCordValue",
    //Data of the RenderContextValue (Content of getValJSON() )
    data: {x: 0.1, y: -0.2},
    //The transformations that have to be noted (Content of getWritableTransformations() )
    wtr: ["de.hcinc.rctx.tr.ScaleOffsetTransofrmation"],
    //The array of transformations in chronological order (Content of getTransformations() )
    tr: [
        {
            ident: "de.hcinc.rctx.tr.ScaleOffsetTransofrmation",    //Identifier of ScaleOffsetTransofrmation
            data: {xs: 2, xy: 2, xo: 0.1, yo: 0.3}                  //Data of the ScaleOffsetTransofrmation
        }
    ]
}
~~~

### RenderContextValue

`RenderContextValue` is the actual value that is transmitted in the `RenderContextObject`.

- It is a normal object for data holding, That can get implementations of different custom methods like getters. For example the `TwoDCordValue` (derived by `RenderContextObjectValue`), which stores 2D-Coordinates, has the Methods `getX()` and `getY()` to get those coordinates.

*NOTE: A RenderContextValue does not do any calculations - it is just a data wrapper class*

The `RenderContextValue` it is just a concept and usually no concrete class, since it is just a data-holder object to hold data, which can be used universally - Programmically it is a `DataPackable` to improve the object-portability


### RenderContextTransformationInformation

Contains a transformation that happened to the `RenderContextObject` to has to be reverted. - It may be in a parsed way stored or just in a json way. It has to be parsed before it can be reverted by an algorithm. It can be checked if it is parsed or not with the `isParsed()` method. - Those objects can also be present in an unparsed state, but they have to be parsed before use, but over that it works similar to the `DataPackable`

*Example-Scenario:* The `RenderContextObject` called `ScaleOffsetTransofrmation` describes a scale and offset of the 2D-Transformtaion. (Scale gets applied before offset)
The `ScaleOffsetTransofrmation` has the values for y/yscale set to 2 the x-offset to 0.1 and the y-offset 0.3
We now have an `RenderContextValue` called `TwoDCordValue` that point to the coordinates x: 0.1 y: -0.2
When the transformation then gets reversed, the resulting `TwoDCordValue` will be x: 0.3 (0.1 * 2 + 0.1) y: 0.4 (0.3 * 2 - 0.2)

Implementation of `RenderContextTransformationInformation`:

- `getIdent()` -> `String` - Gets the identifier for corresponding to the type of `RenderContextValue`.
- `getJson()` -> `RenderContextTransformationInformation` - Exports the object as JSON to be imported by a `RenderContextTransformationInformationImporter` later on.
- `isParsed()` -> `boolean` - Checks if it is parsed, if not it has to be parsed before it gets used by an method.


JSON export example (via `getJson()`) - here the data of a `ScaleOffsetTransofrmation`:

~~~
{xs: 2, xy: 2, xo: 0.1, yo: 0.3}
~~~

#### RenderContextTransformationInformationImporter

Imports `RenderContextTransformationInformation` from the JSON format - It works similar to the `DataPackableImporter`

Implementation of `RenderContextTransformationInformationImporter`

- `getIdent()` -> `String` - Gets the identifier this importer belongs to the `RenderContextTransformationInformation` that this instance of importer imports
- `importObject(String json)` -> `RenderContextTransformationInformation` - Imports the `RenderContextTransformationInformation` from the previously exported JSON


### RenderModifier

A `RenderModifier` is a data-holder class to store a modification of what has to be done in the render process.

It is a normal object for data holding, that contains custom methods like getters. For example the `AffineMatrixTranformation` (derived by `RenderModifier`) has the Method `getMatrix()` -> `double[][]` to get the matrix of the affine 2D-transofrmation.

The Implementation of `RenderModifier` is done over the `DataPackable` to ensure data-portability, but it is still a definite class to ensure that RenderModifiers aren’t confused with other `DataPackable` which don’t symbolize a definite modification.

#### RenderModifierImporter

Imports `RenderModifier` from the JSON format

Implementation of `RenderModifierImporter`

- The object contains an implementation of a `DataPackableImporter` that generates `RenderModifier`


### RenderContextMask

The `RenderContextMask` can...

- ...provide which part of the `RenderContext` is needed for (a) certain operation(s). Here it is mostly optional and should just provide a chance to stop the `RenderContext` from becoming unreasonable big.
- ...be used to mark for which `RenderContext`s a result is valid or can be reused.

Typically the use of the `RenderContextMask` is never mandatory, since it is just a information tool to determine on what data can be left away or which results can be reused.

- It basically contains a map what `RenderContextValue`s are relevant or what `RenderContextValue`s it applies to. All not listed values are assumed as irrelevant.
- The map has string keys which are the same as how the `RenderContextObject`s/`RenderContextValue`s are mapped in the `RenderContext` that should be masked.
- The values in the map define how the mask applies on that key. The Object used for is a `DataPackableMask`.

Tools it provides:

- Masking a `RenderContext`, so it only only contains the the masked values
- Check on a `RenderContext` is included in that mask (for example used in validity-checking, be aware that it is not checked for `RenderModifier`, since the `RenderContextMask` only contains information about the objects/values and not the modifiers)
- The mask-map can be output

Masking Scenarios:

- Included (2): The data is inside the the defined region
    - Masking: The data is going to be included
    - Checking: The data is valid regarding that value
- Undetectable (1): The compared data couldn't be absolutely detected as inside and or outside the defined region
    - Masking: The data is going to be included
    - Checking: The data is not seen as valid regarding that value
- Excluded (0): The compared data is not intersecting with the defined region
    - Masking: The data is allowed to be excluded
    - Checking: The data is not seen as valid regarding that value
- Unmasked (3): There is no DataPackableMask provided for one or more keys in the `RenderContext`
    - Masking: The data is allowed to be excluded
    - Checking: The data is valid regarding that value
- Error (<0): An error occurred 
    - Same behavior as for Undetectable *(Note: It is rather recommended to throw an exception if possible)*

Implementation of `RenderContextMask`

- `maskRCTX(RenderContext rctx)` -> `RenderContext` - Masks the RenderContext to just the needed values. - It can return null if the `RenderContext` is left unchanged
- `maskCheck(RenderContext rctx)` -> `int` - Checks if the mask is containing the values of the given `RenderContext` - Error (<0): An error occurred (An error can also be issued over a thrown exception); Excluded (0): The RenderContext is not completely covered by the mask; Undetectable (1): The mask contains unpredictable behavior, no complete masking guaranteed; Included (2): The mask is covering the `RenderContext` completely; Unmasked (3): The `RenderContext` contains values that aren't mentioned in the `RenderContextMask`, but values that are mentioned in the mask are covered by it.
- `getMaskMap()` -> `Map<String, DataPackableMask>` - Returns the masking map. Keyed by the keys used in the `RenderContext`, with a `DataPackableMask` as a value each, that define if the masking on that value of the `RenderContext`
- `includes()` -> `Pair<Element, String[]>[]` - Lists sub-masks that should also be included to the rest of the mask



## ExecutionInterface

Interface to request render and task-executions.

The render procedure here is based on that what is described for a renderable, you may have a look into the "Executing a render of a Renderable" section under the Renderable-definition.

Methods in `ExecutionInterface`:

- `unpackRenderContextValue(RenderContextObject rctxo)` -> `RenderContextValue` - Unpacks the `RenderContextValue` from an `RenderContextObject` and handles the noted transformations.
- `unpackRenderContextObject(RenderContextObject rctxo, double effort)` - Unpacks the `RenderContextObject` and handles the noted transformations as far as possible with the described effort. (Effort-index - 0: Only Locally; 0&lt;x&lt;1: Effort index, the bigger the higher the effort; 1: Maximum effort, will throw an error if couldn't be unpacked)
- `buildRender(Renderable rnd, String[] segments)` -> `long` - Starts building the execution of a `Renderable`, it can be referenced via the returned `renderId` (NOTE: The defined segments here will limit what data/modifications/render-segments will be available - to select everything insert a `null` into `segments`-argument)
- `getRenderableProperties(long renderId, String[] keys, RenderContext rctx)` -> `RenderableProperties` - Fetch `RenderableProperties` from a renderable that is currently getting built (via `buildRender(Renderable rnd)`)
- `getModificationPromises(long renderId, String[] modificationIdents, RenderContext rctx)` -> `String[]` - Fetch Identifiers of the `RenderableModifier`(s) that are promised to be able to get used.
- `enqueueRender(long renderId, RenderContext rctx, ResultSettings rs, long prio)` -> `int` (status) - Enqueues the prepared render ( that has been constructed via `buildRender(Renderable rnd)`) under the `renderId` with the `RenderContext` and `ResultSettings` to fetch the result later on (via `fetchRenderResult(long renderId)`)
- `enqueueRender(Renderable rend, RenderContext rctx, ResultSettings rs, long prio)` -> long - Directly Enqueues render of Renderable and returns the `renderId` to fetch the result later on
- `fetchRenderResult(long renderId)` -> `RenderResult` - Waits for the Render to finish and returns result
- `enqueueTarget(Target trgt, long prio)` -> long - Enqueues Target Execution returns the `targetId` to fetch the result later on
- `fetchTargetResult(long targetId)` -> `TargetResult` - Waits for the execution of the Target to finish and returns the `TargetResult`



## SystemResourceInterface

The `SystemResourceInterface` displays which manages the reserving of system resources, besides the default of one CPU thread. For example extra cpu threads or GPUs to use.

TODO More infos on implementation of the `SystemResourceInterface`



## Target

The `Target` an object that controls what even has to happen.

- Targets control what renders that are executed
- They control the verbosity level of the computed computation
- The also manage the deployment of the result
- Since Targets are derived from Elements, so they have the same Ready/Dependence/Metadata/etc features

Protocol-wise, returned the target a result object, that packs information about the render, for example info, warnings, errors etc... - eg It can also return another pointer to a resource in the result if a specific result should returned to be executed, or it can manage the result delivery on another way.

Methods in `Target` (not including derived ones from `Element`):

- `getVerbosityLevel()` -> `VerbosityLevel` - Gets the `VerbosityLevel` for the execution of the Target
- `run(SystemResourceInterface sri, ExecutionInterface ei)` -> `TargetResult` - Runs Target
- `getExecutionRequirements()` -> `ExecutionRequirements` - Returns the `ExecutionRequirements` for the `Target`
- `getPipelineSettings()` -> `PipelineSettings` - Gets `PipelineSettings` for this target, which contains information on algorithm picking and attributes recognized by pipleline modules


### TargetResult

Result data of the certain target

TODO More info about the implementation of `TargetResult`



## ExecutionRequirements

`ExecutionRequirements` define what an operation needs to be executed, these settings can be requested from for example the types: `Renderable` and `Target` - These settings should ensure, that the platform resources/capabilities for that Element are set right.

Examples for what can be defined in the `ExecutionRequirements`:

- Required resources
- Required methods
- Required output capabilities (Like access to file systems, network connections, etc...)

Implementation for `ExecutionRequirements`:

- `getMaxSupportedThreads()` -> `int` - Get the maximum supported amount of CPU threads (0 if without limit, 1 default for one thread)
- Custom Compute Device Support coming soon



## PipelineSettings

`PipelineSettings` define usable implementations, algorithms and attributes that are recognized by the pipeline. They are applied to an element to control its compute behavior.

The `PipelineSettings` is a data holder class, it is made up of two major parts the `AlgorithmMap` which contains the information about the usable implementations and algorithms that may be used and the `PipelineAttributeMap`, which contain attributes that can be recognized by `PipelineModules`

Implementation of `PipelineSettings`:

- `getAlgorithmMap()` -> `AlgorithmMap` - Gets the `AlgorithmMap`, which defines usable implementations and algorithms
- `getPipelineAttributeMap()` -> `PipelineAttributeMap` - Gets the `PipelineAttributeMap`, which contains attributes that can be recognized by `PipelineModules`

## AlgorithmMap

This defines the usable implementations for elements and those algorithms and also the usable algorithms for element casting.

It can be contained in the `PipelineSettings` or be emitted by `Renderables`

### Element Algorithms

Those algorithms define in which implementation and algorithm an element should be run.

- If set in the `PipelineSettings` it then is used recursively for all elements that are executed below
- If emitted by a `Renderable` it just applies to the `Renderable` itself, replacing the predefined Element Algorithms defined by the `PipelineSettings` above

> Definition: **element / implementation / algorithm**
>
> - An element is an object which are certain results expected from to do, it is referenced by an `ident` - an identification code (Example ident for an element `de.hcinc.imgc.comp.renderlayer`)
> - An implementation is how the element is implemented - implementations have to be chosen on load and are usually represented by an object in the certain instance (The ident for a implementation may look like this `de.hcinc.imgc.java.comp.renderlayer`, but it may also look like this `com.example.imgcmod.kt.comp.renderlayer` if e.g. the implementation is by a different developer)
> - The algorithm defines how things are processed in an element, different algorithms have for example a different accuracy or other strengths/weaknesses - algorithms can be changed before every processing an element and don’t require re-instantiation like changing the implementation


The data Structure for elements looks like this: The first layer defines the implementation and the second layer the algorithm that is able to be used. The order defines the priority of the implementation/algorithm if an algorithm is seen as compatible, typical behavior would be to pick it and all other available algorithms in the same implementation, which are defined.

Example for `de.hcinc.imgc.comp.renderlayer`:

- `de.hcinc.imgc.java.comp.renderlayer`
    - `de.hcinc.imgc.java.comp.renderlayer.rawcompute.highprec` (1. Solution - Will pick `de.hcinc.imgc.java.comp.renderlayer` implementation and make it executable with the algorithms `de.hcinc.imgc.java.comp.renderlayer.rawcompute.highprec`, `de.hcinc.imgc.java.comp.renderlayer.rawcompute.opengl` (if available) and `de.hcinc.imgc.java.comp.renderlayer.rawcompute.lowprec` (if available))
    - `de.hcinc.imgc.java.comp.renderlayer.rawcompute.opengl` (2. Solution - Will pick `de.hcinc.imgc.java.comp.renderlayer` implementation and make it executable with the algorithms `de.hcinc.imgc.java.comp.renderlayer.rawcompute.opengl` and `de.hcinc.imgc.java.comp.renderlayer.rawcompute.lowprec` (if available))
- `com.example.imgcmod.kt.comp.renderlayer`
    - `com.example.imgcmod.kt.comp.renderlayer.compute.lowprec` (3. Solution - Will pick `com.example.imgcmod.kt.comp.renderlayer` implementation and make it executable with the algorithm `com.example.imgcmod.kt.comp.renderlayer.compute.lowprec`)
- `de.hcinc.imgc.java.comp.renderlayer`
    - `de.hcinc.imgc.java.comp.renderlayer.rawcompute.lowprec` (4. Solution - Will pick `de.hcinc.imgc.java.comp.renderlayer` implementation and make it executable with the algorithm `de.hcinc.imgc.java.comp.renderlayer.rawcompute.lowprec`)

### Casting Algorithms

The allowed casting algorithms to cast elements to fitting input types

- If emitted by a `Target` it then is used recursively for all elements that are executed below
- If emitted by a `Renderable` it just applies to all child executions in a no-recursive manner, replacing the Casting Algorithms defined by the `Target` above

The available algorithms are just displayed as a plain list of identifiers, similar to the one with for the element execution, they just don’t have an implementation since they not have to be loaded into treed elements. - The earlier it is listed in the list, the higher the priority of the algorithm above others that match the casting.

An example would be:

- `de.hcinc.imgc.cast.img.bimgtoexr`
- `de.hcinc.imgc.cast.img.exrtobimg`
- `com.example.imgc.cast.img.bimgtopng`
- `com.example.imgc.cast.img.pngtobimg`

### Implementation

**//TODO Implement all**

Element Algorithms for a specific element are stored as are stored in practice in `Pair<Implementation,Algorithm[]>[]` ...  and seen as in generic data types (like in usual implementation) it stores a `Pair<String,String[]>[]`

Those are then stored in a map keyed by the Element identifiers - resulting in this structure `Map<String, Pair<Implementation,Algorithm[]>[]>`

The casting algorithm list is stored in a simple array: `String[]`

The constructor is then take the two values and stores it into the data-object `AlgorithmMap`

An implementation example (here in java) is:

Constructor:

`public AlgorithmMap(Pair<Implementation,Algorithm[]>[] elemalg, String[] castalg)`

Instantiation example:

~~~
new AlgorithmMap(
    new Pair<String, String[]>[] {
        new Pair<String, String[]>("de.hcinc.imgc.java.comp.renderlayer", new String[] {
            "de.hcinc.imgc.java.comp.renderlayer.rawcompute.highprec",
            "de.hcinc.imgc.java.comp.renderlayer.rawcompute.opengl"
        }),
        new Pair<String, String[]>("com.example.imgcmod.kt.comp.renderlayer", new String[] {
    		"com.example.imgcmod.kt.comp.renderlayer.compute.lowprec"
        }),
        new Pair<String, String[]>("de.hcinc.imgc.java.comp.renderlayer", new String[] {
    		"de.hcinc.imgc.java.comp.renderlayer.rawcompute.lowprec"
        })
    },
    new String[] {
        "de.hcinc.imgc.cast.img.bimgtoexr",
        "de.hcinc.imgc.cast.img.exrtobimg",
        "com.example.imgc.cast.img.bimgtopng",
        "com.example.imgc.cast.img.pngtobimg"
    }
);
~~~

In JSON format it would look like this:

~~~
{
  elemalgo:
  [
    {
      "impl": "de.hcinc.imgc.java.comp.renderlayer",
      "algo": [
        "de.hcinc.imgc.java.comp.renderlayer.rawcompute.highprec",
        "de.hcinc.imgc.java.comp.renderlayer.rawcompute.opengl"
      ]
    },
    {
      "impl": "com.example.imgcmod.kt.comp.renderlayer",
      "algo": [
        "com.example.imgcmod.kt.comp.renderlayer.compute.lowprec"
      ]
    },
    {
      "impl": "de.hcinc.imgc.java.comp.renderlayer",
      "algo": [
        "de.hcinc.imgc.java.comp.renderlayer.rawcompute.lowprec"
      ]
    }
  ],
  castalgo:
  [
    "de.hcinc.imgc.cast.img.bimgtoexr",
    "de.hcinc.imgc.cast.img.exrtobimg",
    "com.example.imgc.cast.img.bimgtopng",
    "com.example.imgc.cast.img.pngtobimg"
  ]
}
~~~


## PipelineModules

`PipleineModules` are components which modify the behavior of the render pipeline to optimize the performance, for example centralizing the computation of elements to improve or to avoid multi-instantiation costs for recursive elements.

**TODOO implementation**




# Utility Definitions



## Cloneable

The cloneable is an interface that enables objects to be copied and still behave with the same results as thier original would have given.

Implementation of `Cloneable`:

- `clone()` -> (this) - Clones the element

## DataDump

A holder-class for raw data

- `getData()` -> `byte[]` - Data in bytes
- `getSize()` -> `int` - Size of the data returned by `getData()`
- `isJson()` -> `boolean` - Returns if the data can be directly put into a JSON


## DataResource

- A `DataResource` is an object that can be packed into a byte-array (and ideally read back into application-memory by a corrosponding `DataResourceImporter`)
- The types of a `DataResource` are referenced by an unique Identifier, which is also the same as the importer used to "materialize" the object again.
- Made cloneable via a `Cloneable` implementation

Implementation of `DataResource`:

- `getIdent()` -> `String` - Get the Identifier of the `DataResource` and its `DataResourceImporter`
- `getData()` -> `DataDump` - Data, which describes the `DataResource` 

### DataResourceImporter

Tool to import a data to a `DataResource`

- `getIdent()` -> `String` - Get the Identifier unique to the `DataResource` structure that will be imported
- `import(DataDump data)` -> `DataResource` - Import the `DataDump` as a `DataResource`


## DataPackable

- `DataPackable`s (with thier `DataPackableImporter`) are implementing a `DataResource`/`DataResourceImporter`, but simplifies the work with simply data structures by storing them in JSON

Implementation of `DataPackable`:

- `load()` (protected) - Load the data (from the data accquired from `getJson()`) into the object.
- `makeJson()` -> `json` (protected) - Create an json with from the data inside the object

Provided functions by the `DataPackable`:

- `getJson()` -> `json` - Provides the json describing the data
- `getSerializedJson()` -> `string` - Provides the serialized-json describing the data
- `ensureLoaded()` (protected) - Ensures that the data has been loaded from the json into the object already (via `load()`)

> ---
>
> **IMPORTANT:**
>
> Make sure `ensureLoaded()` is called before any implementation specific, access data which is orginally provided by the json, so `load()` will be called if necessary
>
> ----

### DataPackableImporter

The `DataPackableImporter` is an extension of the `DataResourceImporter` and provides another method to directly import already parsed json objects:

- `import(json data)` -> `DataPackable`


### DataPackableMask

(TODO: Port over to DataResource)

Defines a mass of `DataPackable`s.

A mask-check done with `maskCheck(DataPackable dp)` has three states:

- Included (2): The `DataPackable` is contained by the mass that is defined.
- Undetectable (1): The `DataPackable` can not be determined to be part of the selected mass.
- Excluded (0): The `DataPackable` is not defined by the defined mass.

There is also an extra state to define there is no mask provided, but this will never get returned by the `maskCheck(DataPackable dp)`-method:

- Unmasked (3): No `DataPackableMask` provided

The `DataPackableMask` is an `DataPackable` itself and also gets imported by an `DataPackableImporter`, it just has one exception: If the field, that usually contains the identifier, contains a `*` a `DataPackableMask` will be generated that will mark everything as included (`DataPackableMaskAll`)

Implementation of `DataPackableMask` (excluding the methods of `DataPackable`):

- `check(DataPackable dp)` -> `int` - Returns mask-check result: 0=Excluded, 1=Undetectable, 2=Included, <0=Error (-3: Error, invalid `DataPackable` that is type-incompatible) - *Note: the `DataPackable` may be `null`*
- `global()` -> `int` - Get a global containment assumption without having to give a concrete value: Excluded (0): never contained; Undetectable (1): no absolute containment; Included (2): always contained

Results of a `DataPackableMaskAll` (including the methods of `DataPackable` and `DataPackableMask`):

- `maskCheck(DataPackable dp)` -> `int` - Always returns a `2` (Included) - Even if the `DataPackable` is null
- `hasAnyCover()` -> `boolean` - Returns `true`
- `getIdent()` -> `String` - (Derived from `DataPackable`) - Returns a `*` as an identifier.
- `getData()` -> `String[][]` - (Derived from `DataPackable`) - Returns an empty array.

~~~~~

+TODO

- Targets

- Target Interrupt over Communication-Channel
- Id namespaces for computation ids
- inline ids

- Computation Resource Management
- (Done) Method assurance in Renderables
- While Render: Predepped or Undepped child task
- (Done) Make Ready with keys to Unready
- (Done) Rename RenderingContext to RenderContext
- Check the ExecutionInterface and other places that reference the ready-state and dependencies of an Element if they apply to the new standard

