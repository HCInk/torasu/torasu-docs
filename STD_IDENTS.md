# TORASU-STD IDENTS

A overview of the identifiers in use by TORASU's standard library

## Pipelines

`STD::PNUM` - Numeric pipeline

`STD::PVIS` - Visual pipeline

`STD::PAUDIO` - Audio pipeline

## Elements/Renderables

`STD::RNUM` - Create a fixed numeric value

`STD::RMULTIPLY` - Multiply two nodes

## DataResources/DataPackables

`STD::DPNUM` - Numeric DataPackable

`STD::DRBIMG` - Buffered image